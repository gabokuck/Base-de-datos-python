import sqlite3

conexion = sqlite3.connect("ejemplo.db")

cursor = conexion.cursor()

#cursor.execute("CREATE TABLE usuarios (nombre VARCHAR(100), EDAD INTEGER, email VARCHAR(100))")
#cursor.execute("INSERT INTO usuarios VALUES ('Gabriel',28,'gabriel@gmail.com')")

#cursor.execute("SELECT * FROM usuarios")
#usuario = cursor.fetchone()
#print(usuario)

#usuarios =  [
#	('Mario', 51, 'mario@gmail.com'),
#	('Mercedes',38,'mercedes@gmail.com'),
#	('Juan',19,'juan@gmail.com')
#]

#cursor.executemany("INSERT INTO usuarios VALUES (?,?,?)", usuarios)

cursor.execute("SELECT * FROM usuarios")

usuarios = cursor.fetchall()

for usuario in usuarios:
	print(usuario)

conexion.commit()
conexion.close()